package com.project.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:/hibernate.properties")
public class RepositoryConfiguration {

    @Value("${hibernate.dialect}")
    String hibernatDialect;

    @Value("${hibernate.show_sql}")
    String hibernatShowSql;

    @Value("${jdbc.driverClassName}")
    String hibernatDriverClassName;

    @Value("${jdbc.url}")
    String JdbcUrl;

    @Value("${jdbc.username}")
    String userName;

    @Value("${jdbc.password}")
    String password;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {

        System.out.println(" in LocalSessionFactoryBean bean");
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setHibernateProperties(hibernateProperties());
        sessionFactory.setPackagesToScan(new String[] { "com.project" });
        return sessionFactory;
    }

    @Bean
    public Properties hibernateProperties() {
        System.out.println(" in hibernateProperties bean");
        Properties properties = new Properties();
        properties.put("hibernate.dialect", hibernatDialect);
        properties.put("hibernate.show_sql", hibernatShowSql);

        properties.put("hibernate.connection.autoReconnect", true);
        properties.put("hibernate.connection.autoReconnectForPools", true);

        properties.put("hibernate.hbm2ddl.auto", "update");
        return properties;
    }

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        System.out.println(" in dataSource bean");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(hibernatDriverClassName);
        dataSource.setUrl(JdbcUrl);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        System.out.println(" in HibernateTransactionManager bean");
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }
}
