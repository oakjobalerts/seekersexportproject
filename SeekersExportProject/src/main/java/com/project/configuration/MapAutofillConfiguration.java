package com.project.configuration;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.gson.GsonBuilder;
import com.project.model.EmailReceiver;
import com.project.model.FtpExportSourcesModel;
import com.project.model.IpTableMapModel;
import com.project.model.TableStructureModel;
import com.project.model.UserToExportOuterModel;
import com.project.repository.FtpExportSourcesModelRepository;
import com.project.repository.UserToExportModelRepository;

@Configuration
public class MapAutofillConfiguration {

    @Autowired
    UserToExportModelRepository userToExportModelRepository;

    @Autowired
    FtpExportSourcesModelRepository ftpExportSourcesModelRepository;

    // @Autowired
    // UserToExportModelService userToExportModelService;

    @Value("${email.receiver.file}")
    String emailReceiverJsonFile;

    @Value("${source.name.table.json.map.file.path}")
    String sourceNameTableFilePath;

    @Value("${table.structure.map.file.path}")
    String tableStrucreFilePath;

    @Value("${user.export.model.json.file.path}")
    String userExportFilePath;

    @Value("${ftp.export.seeker.json.map.file.path}")
    String exportSeekersFilePath;

    @Value("${ip.table.map.model.json.map.file.path}")
    String ipTableMapModelFilePath;

    @Value("${advertisement.table.mapping.file.path}")
    String advertisementTableMapping;

    // @Bean(name = "sourceNameTableListMap")
    // Map<String, List<String>> sourceNameTableListMap() {
    //
    // return userToExportModelRepository.getSourceNameTableListMap(sourceNameTableFilePath);
    // }

    @Bean(name = "sourceNameTableListMap")
    Map<String, List<String>> sourceNameTableListMapDynamically() {

        return userToExportModelRepository.getSourceNameTableListMapDynamicallyFromSeekerImportJason(advertisementTableMapping);
    }

    @Bean(name = "tableStructureMap")
    Map<String, TableStructureModel> tableStructureMap() {

        return userToExportModelRepository.getTableStructureMap(tableStrucreFilePath);
    }

    @Bean(name = "userExportOuterModel")
    UserToExportOuterModel userExportOuterModel() {
        return userToExportModelRepository.getUserExportOuterModel(userExportFilePath);
    }

    @Bean(name = "exportSeekerModelList")
    List<FtpExportSourcesModel> exportSeekerModelList() {
        return ftpExportSourcesModelRepository.createExportSeekersModelList(exportSeekersFilePath);
    }

    @Bean(name = "ipTableMapModel")
    IpTableMapModel getIpTablesMapModel() {
        return userToExportModelRepository.getIpTableMapModel(ipTableMapModelFilePath);
    }

    @Bean(name = "emailReceiverObject")
    public EmailReceiver getEmailReceivers() {

        try {
            Resource resource = new ClassPathResource(emailReceiverJsonFile);
            InputStream resourceInputStream = resource.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(resourceInputStream));

            EmailReceiver emailReceiver = new GsonBuilder().setPrettyPrinting().create().fromJson(reader, EmailReceiver.class);
            return emailReceiver;

        } catch (Exception e) {
            e.printStackTrace();
            return new EmailReceiver();
        }
    }

    @Bean(name = "tableDomainMap")
    public Map<String, String> createTableDomainMapBean() {

        Map<String, String> tableDomainMap = userToExportModelRepository.createTableDomainMapRepository();

        tableDomainMap.put("tbl_job_seekers_paperrosejobs", "paperrosejobs.com");

        return tableDomainMap;
    }

}
