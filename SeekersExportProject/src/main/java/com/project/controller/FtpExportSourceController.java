package com.project.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.opencsv.CSVWriter;
import com.project.model.CsvExportModel;
import com.project.model.FtpExportSourcesModel;
import com.project.service.FtpExportSourcesModelService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

@Controller
public class FtpExportSourceController {

    @Autowired
    FtpExportSourcesModelService ftpExportSourcesModelService;

    public CSVWriter createCsvWriter(FtpExportSourcesModel exportSourcesModel, String folderPath, String fileName) {

        return ftpExportSourcesModelService.createCsvWriter(exportSourcesModel, folderPath, fileName);
    }

    public void createCsvFileLineByLine(List<CsvExportModel> csvExportModelList, CSVWriter csvWriter, String ftpsource) {

        for (CsvExportModel csvExportModel : csvExportModelList) {
            ftpExportSourcesModelService.createCsvFileLineByLine(csvExportModel, csvWriter, ftpsource);

        }
    }

    public void closeCsvWriter(CSVWriter csvWriter) {
        ftpExportSourcesModelService.closeCsvWriter(csvWriter);
    }

    public List<CsvExportModel> hitFtpExportApi(List<CsvExportModel> csvExportModelList, FtpExportSourcesModel exportSourcesModel) {

        List<CsvExportModel> csvExportModelsUpdatedWithResponse = new ArrayList<CsvExportModel>();

        HttpClient httpClient = HttpClients.createDefault();

        for (CsvExportModel csvExportModel : csvExportModelList) {

            try {
                csvExportModelsUpdatedWithResponse.add(ftpExportSourcesModelService.writeToFtpExportThroughApi(csvExportModel, exportSourcesModel, httpClient));
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return csvExportModelsUpdatedWithResponse;
    }

    public void uploadFileOverFtpServer(FtpExportSourcesModel exportSourcesModel, String folderPath, String fileName) {

        ftpExportSourcesModelService.uploadFileOverFtpServer(exportSourcesModel, folderPath, fileName);
    }

    public List<CsvExportModel> zrExportAPI(List<CsvExportModel> csvExportModelList, FtpExportSourcesModel exportSourcesModel) {

        List<CsvExportModel> csvExportModelsUpdatedWithResponse = new ArrayList<CsvExportModel>();
        Client client = new Client();
        WebResource webResource = client.resource(exportSourcesModel.getBaseUrl());

        for (CsvExportModel csvExportModel : csvExportModelList) {

            try {
                csvExportModelsUpdatedWithResponse.add(ftpExportSourcesModelService.ZrExportApi(csvExportModel, exportSourcesModel, webResource));
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return csvExportModelsUpdatedWithResponse;
    }

}
