package com.project.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;
import com.opencsv.CSVWriter;
import com.project.model.CsvExportModel;
import com.project.model.FtpExportSourcesModel;
import com.project.model.UserToExportModel;
import com.project.utility.Utility;

@Controller
public class HomeController {

    @Autowired
    List<FtpExportSourcesModel> exportSeekerModelList;

    @Autowired
    UsersToExportController usersToExportController;

    @Autowired
    FtpExportSourceController ftpExportSourceController;

    @Autowired
    StatsController statsController;

    @Autowired
    Utility utility;

    @Value("${db.query.limit.value}")
    Integer globalLimit;

    @Value("${runtest.condition}")
    boolean isRunTest;

    public void startProcess() {

        // usersToExportController.createTableDomainMap();

        System.out.println("Process Started ....");

        for (FtpExportSourcesModel exportSourcesModel : exportSeekerModelList) {

            int limit = globalLimit;

            System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
            boolean isMaxMonthlyCountReached = false;
            String currentExportSourceName = exportSourcesModel.getSourceName();
            Integer totalSentCurrentCount = statsController.getMaxMonthlyTotalSentCount(currentExportSourceName);

            CSVWriter csvWriter = null;

            String folderPath = utility.getcsvFolderPathFromExportSource(currentExportSourceName);
            String fileName = utility.getcsvFileNameFromExportSource(currentExportSourceName);

            boolean isJsonDomainAdded = false;

            if (exportSourcesModel.isActive()) {

                if (exportSourcesModel.getMaxMonthlyCount() != 0
                        && exportSourcesModel.getMaxMonthlyCount() >= totalSentCurrentCount) {
                    isMaxMonthlyCountReached = false;
                }

                csvWriter = ftpExportSourceController.createCsvWriter(exportSourcesModel, folderPath, fileName);
                System.out.println("CSVWriter Created for : " + currentExportSourceName);

                Map<String, String> emailIpMap = new HashMap<String, String>();

                System.out.println(new Gson().toJson(exportSourcesModel));

                List<UserToExportModel> userExportModelFinalWithQueries = usersToExportController.createUserModelQueries(exportSourcesModel);
                System.out.println("User export queries created for : " + currentExportSourceName);
                System.out.println("");

                if (!isRunTest) {
                    // Insert into backup table
                    usersToExportController.insertFromUsersTableIntoSeekerBackupTable(userExportModelFinalWithQueries);
                }
                

                Integer dailyLimitCount = 0;

                // Set Random daily limit for ZR
                if (exportSourcesModel.getSourceName().equals("ZR")) {
                    int min = 3500;
                    int max = 5600;
                    Random rand = new Random();
                    int rndNum = rand.nextInt((max - min) + 1) + min;
                    exportSourcesModel.setDailyLimit(rndNum);
                }

                // Set Random daily limit for Jobtome
                if (exportSourcesModel.getSourceName().equals("JobtoMe")) {
                    int min = 11000;
                    int max = 25000;
                    Random rand = new Random();
                    int rndNum = rand.nextInt((max - min) + 1) + min;
                    exportSourcesModel.setDailyLimit(rndNum);
                }
                
                // Set Random daily limit for DPCC
                if (exportSourcesModel.getSourceName().equals("DPCC")) {
                    int min = 10000;
                    int max = 16000;
                    Random rand = new Random();
                    int rndNum = rand.nextInt((max - min) + 1) + min;
                    exportSourcesModel.setDailyLimit(rndNum);
                }
                
                // Set Random daily limit for DMS
                if (exportSourcesModel.getSourceName().equals("DMS")) {
                    int min = 10000;
                    int max = 20000;
                    Random rand = new Random();
                    int rndNum = rand.nextInt((max - min) + 1) + min;
                    exportSourcesModel.setDailyLimit(rndNum);
                }
                
                

                for (UserToExportModel userToExportModel : userExportModelFinalWithQueries) {

                    Integer userCountForCurrentSource = 0;
                    Integer start = 0;
                    Integer dailyLimit = exportSourcesModel.getDailyLimit();

                    if (dailyLimit == null) {
                        dailyLimit = 0;
                    }
                    if (dailyLimit != 0 && limit > dailyLimit) {
                        limit = dailyLimit;
                    }

                    if (dailyLimit != 0 && start + limit > dailyLimit) {
                        limit = dailyLimit - start;
                    }

                    if (dailyLimit != 0 && dailyLimitCount >= dailyLimit) {
                        continue;
                    }

                    System.out.println("Fetchinng datain in : " + userToExportModel.getCurrentTable() + " for : " + userToExportModel.getSourceName() + " start : " + 0 + " limit  : " + limit);

                    System.out.println("");

                    List<CsvExportModel> csvExportModelList = usersToExportController.createPaginatedCsvFileFromDatabase(userToExportModel, 0, limit);
                    userCountForCurrentSource = csvExportModelList.size();
                    dailyLimitCount = dailyLimitCount + userCountForCurrentSource;

                    // TODO Add Jason domain here
                    if (!isJsonDomainAdded && userCountForCurrentSource > 0) {
                        csvExportModelList = usersToExportController.addJsonDomainToExportFile(csvExportModelList, userToExportModel, exportSourcesModel.getTrackingName());
                        isJsonDomainAdded = true;
                    }

                    System.out.println("Db result size of Users to export : " + csvExportModelList.size());
                    System.out.println("");

                    // TODO Check if IP is required and create map by fetching
                    // ip from IP(click,open) tables
                    if (exportSourcesModel.isIpRequired() && csvExportModelList.size() > 0) {

                        System.out.println("IP is required, fetching ip in IP click/open log tables..........");
                        System.out.println("");
                        emailIpMap = usersToExportController.createEmailIpMap(userToExportModel, exportSourcesModel);
                    }

                    while (csvExportModelList != null && csvExportModelList.size() > 0) {

                        start = start + limit;

                        totalSentCurrentCount = totalSentCurrentCount + csvExportModelList.size();

                        if (exportSourcesModel.getMaxMonthlyCount() != 0
                                && totalSentCurrentCount >= exportSourcesModel.getMaxMonthlyCount()) {
                            isMaxMonthlyCountReached = true;
                        }

                        // TODO Update Ip
                        if (exportSourcesModel.isIpRequired()) {
                            csvExportModelList = usersToExportController.updateIpAddress(csvExportModelList, emailIpMap);
                        }

                        // TODO Handle url export here
                        if (exportSourcesModel.isUrlExport()) {		// && !isRunTest
                            if (exportSourcesModel.getSourceName().equals("ZR")) {
                                csvExportModelList = ftpExportSourceController.zrExportAPI(csvExportModelList, exportSourcesModel);
                            } else { // (exportSourcesModel.getSourceName().equals("L5"))
                                csvExportModelList = ftpExportSourceController.hitFtpExportApi(csvExportModelList, exportSourcesModel);
                            }
                        }

                        ftpExportSourceController.createCsvFileLineByLine(csvExportModelList, csvWriter, exportSourcesModel.getSourceName());

                        // TODO ADD delete here
                        usersToExportController.deleteIds(csvExportModelList);

                        if (dailyLimit != 0 && dailyLimitCount >= dailyLimit) {
                            csvExportModelList.clear();
                        }

                        if (dailyLimit != 0 && start + limit > dailyLimit) {
                            limit = dailyLimit - start;
                        }

                        if (isRunTest) {
                            csvExportModelList.clear();
                        }
                        // TODO If result size is less that limit, no need to
                        // hit again as pagination is complete
                        if (csvExportModelList.size() < limit) {
                            csvExportModelList.clear();
                        } else {
                            System.out.println("Fetchinng datain in : " + userToExportModel.getCurrentTable() + " for : " + userToExportModel.getSourceName() + " start : " + 0 + " limit  : " + limit);
                            System.out.println("");

                            csvExportModelList = usersToExportController.createPaginatedCsvFileFromDatabase(userToExportModel, 0, limit);
                            userCountForCurrentSource = userCountForCurrentSource + csvExportModelList.size();
                            dailyLimitCount = dailyLimitCount + csvExportModelList.size();

                            System.out.println("Db result size of Users to export : " + csvExportModelList.size());
                            System.out.println("");
                        }
                    }
                    statsController.writeStatsSourceWise(currentExportSourceName, userToExportModel.getSourceName(), userCountForCurrentSource, isMaxMonthlyCountReached);

                }

                System.out.println("Closing CSVWriter");
                System.out.println("");
                ftpExportSourceController.closeCsvWriter(csvWriter);

                statsController.sendSmtpMail(exportSourcesModel, isMaxMonthlyCountReached);

                // TODO UPLOAD TO FTP SERVER HERE
                if (!isMaxMonthlyCountReached && !isRunTest) {
                    ftpExportSourceController.uploadFileOverFtpServer(exportSourcesModel, folderPath, fileName);
                }
            }
        }

    }
}

// if (exportSourcesModel.getMaxMonthlyCount() == 0 ||
// exportSourcesModel.getMaxMonthlyCount() > totalSentCurrentCount) {
// isMaxMonthlyCountReached = false;
// // if (exportSourcesModel.getMaxMonthlyCount() - totalSentCurrentCount <
// limit && exportSourcesModel.getMaxMonthlyCount() != 0) {
// // limit = exportSourcesModel.getMaxMonthlyCount() - totalSentCurrentCount;
// // }
// }

// if (userCountForCurrentSource + totalSentCurrentCount >
// exportSourcesModel.getMaxMonthlyCount() &&
// exportSourcesModel.getMaxMonthlyCount() != 0) {
// isMaxMonthlyCountReached = true;
// // break;
// }
// if (exportSourcesModel.getMaxMonthlyCount() - totalSentCurrentCount < limit)
// {
// limit = exportSourcesModel.getMaxMonthlyCount() - totalSentCurrentCount;
// }