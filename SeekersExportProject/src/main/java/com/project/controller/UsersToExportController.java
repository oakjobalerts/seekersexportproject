package com.project.controller;

import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;
import com.project.model.CsvExportModel;
import com.project.model.FtpExportSourcesModel;
import com.project.model.IpTableMapModel;
import com.project.model.UserToExportModel;
import com.project.model.UserToExportOuterModel;
import com.project.service.UserToExportModelService;
import com.project.utility.Utility;

@Controller
public class UsersToExportController {

    @Autowired
    UserToExportModelService userToExportModelService;

    @Autowired
    Map<String, List<String>> sourceNameTableListMap;

    @Value("${jason.tracking.domain.name}")
    String jasonTrackingDomainName;

    @Autowired
    Utility utility;
    

    // @Autowired
    // Map<String, TableStructureModel> tableStructureMap;

    @Autowired
    UserToExportOuterModel userExportOuterModel;

    @Autowired
    IpTableMapModel ipTableMapModel;

    public void insertFromUsersTableIntoSeekerBackupTable(List<UserToExportModel> userExportModelFinalWithQueries) {

        userToExportModelService.insertIntoSeekerBackupTableService(userExportModelFinalWithQueries);
    }

    public List<UserToExportModel> createUserModelQueries(FtpExportSourcesModel ftpExportSourcesModel) {

        List<UserToExportModel> userExportModelFinalWithQueries = userToExportModelService.createUserToExportListWithCompleteTableQueries(
                userExportOuterModel, sourceNameTableListMap, ftpExportSourcesModel.getInterval(), ftpExportSourcesModel.isFirstTimeExport(), ftpExportSourcesModel.getSourceName());

        return userExportModelFinalWithQueries;
    }

    public List<CsvExportModel> createPaginatedCsvFileFromDatabase(UserToExportModel userToExportModel, Integer start, Integer limit) {

        List<CsvExportModel> result = userToExportModelService.getUserFromDatabaseSourceWise(userToExportModel, start, limit);

        return result;
    }

    public Map<String, String> createEmailIpMap(UserToExportModel userToExportModel, FtpExportSourcesModel ftpExportSourcesModel) {

        // GET EMAIL IP MAP HERE
        // List<String> providerTableList = sourceNameTableListMap.get(userToExportModel.getSourceName());
        Map<String, String> emailIpMap = userToExportModelService.getEmailIpMap(ipTableMapModel, userToExportModel, ftpExportSourcesModel);
        return emailIpMap;
    }

    public List<CsvExportModel> updateIpAddress(List<CsvExportModel> csvExportModelList, Map<String, String> emailIpMap) {

        // PASS EMAIL IP MAP AND UPDATE IP HERE
        List<CsvExportModel> updatedCsvModelWithIp = userToExportModelService.updateIpAddressFromEmailIpMap(csvExportModelList, emailIpMap);

        return updatedCsvModelWithIp;
    }

    public List<CsvExportModel> addJsonDomainToExportFile(List<CsvExportModel> csvExportModelList, UserToExportModel userModel, String trackingName) {

        String jasonDomain = "_" + utility.getDateMonthINNumber() + trackingName + jasonTrackingDomainName;

        int randomInsertIndex = new Random().nextInt((csvExportModelList.size()));
        int randomSelectIndex = new Random().nextInt((csvExportModelList.size()));

        CsvExportModel csvExportModel = new Gson().fromJson(new Gson().toJson(csvExportModelList.get(randomSelectIndex)), CsvExportModel.class);
        String userEmail = csvExportModel.getEmail().split("@")[0];
        csvExportModel.setEmail(userEmail + jasonDomain);

        csvExportModelList.add(randomInsertIndex, csvExportModel);

        return csvExportModelList;
    }

    // TODO CALL DELETE SERVICE HERE
    public void deleteIds(List<CsvExportModel> csvExportModelList) {
    	userToExportModelService.deleteSeekerExportByIdList(csvExportModelList);
    }

    // public Map<String, String> createTableDomainMap() {
    //
    // return userToExportModelService.createTableDomainMapService();
    // }

}
