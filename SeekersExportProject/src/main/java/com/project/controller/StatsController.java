package com.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import com.project.model.EmailReceiver;
import com.project.model.FtpExportSourcesModel;
import com.project.model.StatsOuterModel;
import com.project.service.EmailSender;
import com.project.service.StatsService;
import com.project.utility.Utility;

@Controller
public class StatsController {

    @Autowired
    StatsService statsService;

    @Autowired
    EmailSender emailSender;

    @Autowired
    Utility utility;

    @Autowired
    @Qualifier("emailReceiverObject")
    EmailReceiver emailReceiver;

    public StatsOuterModel getStatsModel(String source) {

        String path = statsService.getCurrentStatsFullFilePath(source);
        StatsOuterModel statsModel = statsService.getStatsModel(path);

        return statsModel;
    }

    public Integer getMaxMonthlyTotalSentCount(String source) {

        StatsOuterModel statsOuterModel = getStatsModel(source);
        Integer totalSent = statsOuterModel.getTotalSent();
        return totalSent;
    }

    public void writeStatsSourceWise(String exportsourceName, String userSourceName, Integer count, boolean isMaxMonthlyCountReached) {

        String path = statsService.getCurrentStatsFullFilePath(exportsourceName);

        StatsOuterModel statsOuterModel = statsService.updateStatsModel(exportsourceName, userSourceName, count, path, isMaxMonthlyCountReached);
        statsService.writeStatsModelInFile(path, statsOuterModel);
    }

    public void updateMaxLimitReachedDate(String exportsourceName) {

        String path = statsService.getCurrentStatsFullFilePath(exportsourceName);
        StatsOuterModel statsModel = statsService.getStatsModel(path);

        String maxLimitReachedDate = statsModel.getMaxLimitReachedDate();

        if (maxLimitReachedDate == null || maxLimitReachedDate.equals("")) {
            maxLimitReachedDate = utility.getTodayDate();
            statsModel.setMaxLimitReachedDate(maxLimitReachedDate);
            statsService.writeStatsModelInFile(path, statsModel);
        }

    }

    public void sendSmtpMail(FtpExportSourcesModel exportSourcesModel, boolean isMaxMonthlyCountReached) {

        StatsOuterModel statsOuterModel = getStatsModel(exportSourcesModel.getSourceName());

        String subject = exportSourcesModel.getSourceName() + " opener/clicker stats for " + utility.getTodayDate();
        if (isMaxMonthlyCountReached) {
            subject = exportSourcesModel.getSourceName() + " opener/clicker reached max Monthly count";
        }
        String body = emailSender.createEmailBody(statsOuterModel, exportSourcesModel.getMaxMonthlyCount());

        emailSender.sendSmtpEmail(subject, body, emailReceiver);
    }

}
