package com.project.service;

import com.project.model.EmailReceiver;
import com.project.model.StatsOuterModel;

public interface EmailSender {

    public String sendSmtpEmail(String subject, String body, EmailReceiver emailReceiver);

    String createEmailBody(StatsOuterModel statsOuterModel, Integer maxLimit);
}
