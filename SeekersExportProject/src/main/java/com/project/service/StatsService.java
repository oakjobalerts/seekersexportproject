package com.project.service;

import com.project.model.StatsOuterModel;

public interface StatsService {

    String getCurrentStatsFullFilePath(String source);

    StatsOuterModel getStatsModel(String path);

    void writeStatsModelInFile(String filePath, StatsOuterModel statsOuterModel);

    StatsOuterModel updateStatsModel(String exportsourceName, String userSourceName, Integer count, String path, boolean isMaxMonthlyCountReached);
}
