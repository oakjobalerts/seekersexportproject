package com.project.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.GsonBuilder;
import com.project.model.StatsOuterModel;
import com.project.model.StatsSourceWiseInnerModel;
import com.project.service.StatsService;
import com.project.utility.Utility;

@Service
public class StatsServiceImpl implements StatsService {

    @Value("${user.count.stats.folder.path}")
    String statsFolderPath;

    @Autowired
    Utility utility;

    @Override
    public StatsOuterModel getStatsModel(String path) {

        StatsOuterModel statsOuterModel = new StatsOuterModel();
        try {
            FileReader fileReader = new FileReader(path + "");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            statsOuterModel = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, StatsOuterModel.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (statsOuterModel == null) {

            statsOuterModel = createEmptyOuterModel();
            writeStatsModelInFile(path, statsOuterModel);

            // statsOuterModel = new StatsOuterModel();
            // statsOuterModel.setStatsModelMap(new HashMap<String, StatsSourceWiseInnerModel>());
        }

        return statsOuterModel;
    }

    @Override
    public void writeStatsModelInFile(String filePath, StatsOuterModel statsOuterModel) {

        String statsJson = new GsonBuilder().serializeNulls().setPrettyPrinting().create().toJson(statsOuterModel);

        try {
            OutputStreamWriter outputStreamWriter = new FileWriter(filePath);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(statsJson);
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getCurrentStatsFullFilePath(String source) {

        String path = statsFolderPath + File.separator + utility.getCurretYear() + File.separator + source;
        String fileName = utility.getCurretMonth() + "-" + utility.getCurretYear() + "-" + source + "-" + "stats.json";

        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }

        String fullFilePathString = path + File.separator + fileName;

        File fullFilePath = new File(fullFilePathString);

        if (!fullFilePath.exists()) {
            try {
                fullFilePath.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fullFilePathString;

    }

    @Override
    public StatsOuterModel updateStatsModel(String exportsourceName, String userSourceName, Integer count, String path, boolean isMaxMonthlyCountReached) {

        StatsOuterModel statsOuterModel = getStatsModel(path);
        Integer totalSent = statsOuterModel.getTotalSent();
        totalSent = totalSent + count;
        String currentDate = utility.getTodayDate();
        int totalCountAfterMaxLimit = 0;

        Map<String, Integer> sourceWiseCountAfterMaxLimit = statsOuterModel.getSourceWiseCountAfterMaxLimit();

        String maxLimitReachedDate = statsOuterModel.getMaxLimitReachedDate();
        if (isMaxMonthlyCountReached) {
            if (maxLimitReachedDate == null || maxLimitReachedDate.equals("")) {
                maxLimitReachedDate = utility.getTodayDate();
                statsOuterModel.setMaxLimitReachedDate(maxLimitReachedDate);

            }
            totalCountAfterMaxLimit = statsOuterModel.getTotalCountAfterMaxLimit();
            totalCountAfterMaxLimit = totalCountAfterMaxLimit + count;
            
            totalSent = totalSent - count;
            Integer sourceCountAfterLimit = sourceWiseCountAfterMaxLimit.get(userSourceName);

            if (sourceCountAfterLimit != null && !sourceCountAfterLimit.equals("")) {
                sourceWiseCountAfterMaxLimit.put(userSourceName, sourceCountAfterLimit + count);
            } else {
                sourceWiseCountAfterMaxLimit.put(userSourceName, count);
            }
        }

        Map<String, Integer> totalSentDateWise = statsOuterModel.getTotalSentDateWise();

        Integer totalCountForTodayDate = totalSentDateWise.get(currentDate);
        if (totalCountForTodayDate == null) {
            totalCountForTodayDate = 0;
        }
        totalCountForTodayDate = totalCountForTodayDate + count;

        Map<String, StatsSourceWiseInnerModel> statsMapModel = statsOuterModel.getStatsModelMap();

        StatsSourceWiseInnerModel statsModel = statsMapModel.get(userSourceName);

        if (statsModel == null) {
            statsModel = new StatsSourceWiseInnerModel();
        }

        Integer subUserTotal = statsModel.getSubUserTotalSent();
        subUserTotal = subUserTotal + count;

        Map<String, Integer> dateWiseCountMap = statsModel.getDateWiseCount();

        if (dateWiseCountMap == null) {
            dateWiseCountMap = new HashMap<String, Integer>();
        }

        Integer todaySentCount = dateWiseCountMap.get(currentDate);
        if (todaySentCount == null) {
            todaySentCount = 0;
        }

        todaySentCount = todaySentCount + count;

        dateWiseCountMap.put(currentDate, todaySentCount);

        statsModel.setDateWiseCount(dateWiseCountMap);
        statsModel.setSubUserTotalSent(subUserTotal);

        statsMapModel.put(userSourceName, statsModel);

        totalSentDateWise.put(currentDate, totalCountForTodayDate);

        statsOuterModel.setTotalSent(totalSent);
        statsOuterModel.setDate(utility.getCurretMonth() + "-" + utility.getCurretYear());
        statsOuterModel.setExportToSourceName(exportsourceName);
        statsOuterModel.setTotalSentDateWise(totalSentDateWise);
        statsOuterModel.setSourceWiseCountAfterMaxLimit(sourceWiseCountAfterMaxLimit);
        statsOuterModel.setTotalCountAfterMaxLimit(totalCountAfterMaxLimit);
        
        return statsOuterModel;

    }

    public StatsOuterModel createEmptyOuterModel() {

        StatsSourceWiseInnerModel innerModel = new StatsSourceWiseInnerModel();
        innerModel.setSubUserTotalSent(0);
        innerModel.setDateWiseCount(new HashMap<String, Integer>());

        Map<String, StatsSourceWiseInnerModel> innerMap = new HashMap<String, StatsSourceWiseInnerModel>();

        StatsOuterModel statsOuterModel = new StatsOuterModel();
        statsOuterModel.setStatsModelMap(innerMap);
        statsOuterModel.setTotalSentDateWise(new HashMap<String, Integer>());
        statsOuterModel.setSourceWiseCountAfterMaxLimit(new HashMap<String, Integer>());
        statsOuterModel.setTotalCountAfterMaxLimit(0);
        return statsOuterModel;

    }

}
