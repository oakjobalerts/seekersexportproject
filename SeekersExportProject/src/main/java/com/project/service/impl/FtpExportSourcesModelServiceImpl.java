package com.project.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.core.MediaType;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.opencsv.CSVWriter;
import com.project.model.CsvExportModel;
import com.project.model.FtpExportSourcesModel;
import com.project.model.temp.ZrModel;
import com.project.service.FtpExportSourcesModelService;
import com.project.utility.Utility;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class FtpExportSourcesModelServiceImpl implements FtpExportSourcesModelService {

    @Value("${email.url.parameter.replacement.value.key}")
    String emailUrlParameterReplacement;

    @Value("${keyword.url.parameter.replacement.value.key}")
    String keywordUrlParameterReplacement;

    @Value("${first.name.url.parameter.replacement.value.key}")
    String firstNameUrlParameterReplacement;

    @Value("${last.name.url.parameter.replacement.value.key}")
    String lastNameUrlParameterReplacement;

    @Value("${zipcode.url.parameter.replacement.value.key}")
    String zipcodeUrlParameterReplacement;

    @Value("${domain.url.parameter.replacement.value.key}")
    String domainUrlParameterReplacement;

    @Value("${ip.address.url.parameter.replacement.value.key}")
    String ipAddressUrlParameterReplacement;

    @Value("${timestamp.url.parameter.replacement.value.key}")
    String timestampUrlParameterReplacement;

    @Value("${regDate.url.parameter.replacement.value.key}")
    String regDateUrlParameterReplacement;
    
    

    @Autowired
    Utility utility;

    @Override
    public CSVWriter createCsvWriter(FtpExportSourcesModel exportSourcesModel, String folderPath, String fileName) {

        // String folderPath = server27FolderPath + File.separator + exportSourcesModel.getSourceName();
        // String fileName = File.separator + "firebrickgroup_" + todatDate + ".csv";

        CSVWriter csvWriter = null;

        try {

            File file = new File(folderPath);
            if (!file.exists()) {
                boolean directoeyCreated = file.mkdirs();
                System.out.println("directoryCreated : " + directoeyCreated);
            }

            boolean isFirstLineAlreadyWritten = false;

            File filePathWithName = new File(folderPath + fileName);
            if (filePathWithName.exists()) {
                isFirstLineAlreadyWritten = true;
            }

            csvWriter = new CSVWriter(new FileWriter(folderPath + fileName, true));

            if (!isFirstLineAlreadyWritten) {

                if (exportSourcesModel.getSourceName().toLowerCase().equals("rm")) {
                    String[] firstRow = { "Name", "Email", "Location", "Keyword", "Zipcode", "Ip Address", "Url", "Date Time" };
                    csvWriter.writeNext(firstRow);
                } else if (exportSourcesModel.getSourceName().toLowerCase().equals("jj")) {
                    String[] firstRowJJ = { "Email", "Keyword", "Location" };
                    csvWriter.writeNext(firstRowJJ);
                } else { // if (!exportSourcesModel.getSourceName().toLowerCase().equals("jj"))
                    String[] firstRow = { "Name", "Email", "Location", "Keyword", "Zipcode" };
                    csvWriter.writeNext(firstRow);
                }

            }
            csvWriter.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return csvWriter;
    }

    @Override
    public void createCsvFileLineByLine(CsvExportModel csvExportModel, CSVWriter csvWriter, String ftpsource) {

        if (csvExportModel.getEmail() == null || csvExportModel.getEmail().equals("")) {
            return;
        }

        try {

            if (ftpsource.toLowerCase().equals("jj")) {
                csvWriter.writeNext(csvExportModel.toCustomJJArray());

            } else if (ftpsource.toLowerCase().equals("rm")) {
                csvWriter.writeNext(csvExportModel.toArrayRm());
            }
            else {
                csvWriter.writeNext(csvExportModel.toArray());
            }

            csvWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closeCsvWriter(CSVWriter csvWriter) {

        try {
            csvWriter.flush();
            csvWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public CsvExportModel writeToFtpExportThroughApi(CsvExportModel csvExportModel, FtpExportSourcesModel exportSourcesModel, HttpClient httpClient) {

        String url = exportSourcesModel.getUrlParameters();

        try {
            url = url.replace(emailUrlParameterReplacement, utility.nullCheck(csvExportModel.getEmail()).trim());
            url = url.replace(keywordUrlParameterReplacement, utility.nullCheck(csvExportModel.getKeyword()).trim());
            
            if(exportSourcesModel.getSourceName().equals("JobtoMe")){
            	url = url.replace(firstNameUrlParameterReplacement, utility.nullCheck(csvExportModel.getFirstName()).trim()+" "+csvExportModel.getLastName().trim());
            }else{
            	url = url.replace(firstNameUrlParameterReplacement, utility.nullCheck(csvExportModel.getFirstName()).trim());
            }
            
            url = url.replace(lastNameUrlParameterReplacement, utility.nullCheck(csvExportModel.getLastName()).trim());
            url = url.replace(zipcodeUrlParameterReplacement, utility.nullCheck(csvExportModel.getZipcode()).trim());
            url = url.replace(domainUrlParameterReplacement, utility.nullCheck(csvExportModel.getDomain()).trim());
            url = url.replace(ipAddressUrlParameterReplacement, utility.nullCheck(csvExportModel.getIp()).trim());
            url = url.replace(regDateUrlParameterReplacement, utility.nullCheck(csvExportModel.getRegDate()).trim());
            url = url.replace("{curr_date}", utility.get48hrsErlierDate());
            
            
            String l5Date = utility.convertDateFormatForL5Export(utility.nullCheck(csvExportModel.getOpenAtTimestamp() + "").trim());
            url = url.replace(timestampUrlParameterReplacement, l5Date);

            // url = URLEncoder.encode(url, "UTF-8");
            url = exportSourcesModel.getBaseUrl() + url;

            url = url.replace(" ", "%20");           
     
            
            String response = hitUrlGetMethod(url, httpClient);
            csvExportModel.setResponse(response);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return csvExportModel;
    }

    public String hitUrlGetMethod(String myUrl, HttpClient httpClient) {

        HttpResponse response;
        String result = "";
        try {

            HttpGet httpGet = new HttpGet(myUrl);

            response = httpClient.execute(httpGet);
            result = EntityUtils.toString(response.getEntity());

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result.toLowerCase().contains("success")) {
            return "success";
        }

        return result;

    }

    @Override
    public FTPClient createFtpConnection(String host, String user, String pass, int port) {

        FTPClient ftpClient = null;
        try {

            ftpClient = new FTPClient();
            ftpClient.connect(host, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ftpClient;
    }

    @Override
    public void uploadFileOverFtpServer(FtpExportSourcesModel exportSourcesModel, String folderPath, String fileName) {

        FTPClient ftpClient = new FTPClient();

        String server = exportSourcesModel.getFtpName();
        int port = 21;
        String user = exportSourcesModel.getFtpUser();
        String pass = exportSourcesModel.getFtpPassword();

        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();

            File localFile = new File(folderPath + fileName);

            String folder = "/";
            String firstRemoteFile = fileName;

            ftpClient.changeWorkingDirectory(folder);
            int replyCode = ftpClient.getReplyCode();

            if (replyCode != 250) {
                ftpClient.makeDirectory(folder);
            }

            InputStream inputStream = new FileInputStream(localFile);

            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            inputStream.close();
            if (done) {
                System.out.println("The first file is uploaded successfully.");
            }

            inputStream.close();

            if (ftpClient.isConnected()) {
                ftpClient.logout();
                ftpClient.disconnect();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public CsvExportModel ZrExportApi(CsvExportModel csvExportModel, FtpExportSourcesModel exportSourcesModel, WebResource webResource) {

        ZrModel zrModel = new ZrModel();

        zrModel.setEmail(csvExportModel.getEmail());
        zrModel.setCreate_time(csvExportModel.getTimestamp());
        zrModel.setLocation(csvExportModel.getZipcode());
        zrModel.setSearch(csvExportModel.getKeyword());
        String zrModelString = new Gson().toJson(zrModel);

        ClientResponse clientResponse = webResource
                .header("Authorization", "Basic " + exportSourcesModel.getAuthKey())
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, zrModelString);

        JsonObject clientResponseString = new Gson().fromJson(clientResponse.getEntity(String.class), JsonObject.class);
        System.out.println(clientResponseString);

        csvExportModel.setResponse(utility.nullCheck(clientResponseString.toString()));

        return csvExportModel;
    }

}
