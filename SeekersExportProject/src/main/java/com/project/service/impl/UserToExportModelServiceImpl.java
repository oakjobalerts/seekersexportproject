package com.project.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.project.model.CsvExportModel;
import com.project.model.FtpExportSourcesModel;
import com.project.model.IpTableMapModel;
import com.project.model.TableStructureModel;
import com.project.model.UserToExportModel;
import com.project.model.UserToExportOuterModel;
import com.project.repository.UserToExportModelRepository;
import com.project.service.UserToExportModelService;

@Service
public class UserToExportModelServiceImpl implements UserToExportModelService {

    @Autowired
    UserToExportModelRepository userToExportModelRepository;

    @Value("${general.table.structure.key.name}")
    String generalTableStructureKeyName;

    @Value("${firstname.replacement.value.key}")
    String firstNameReplacementValueKey;

    @Value("${lastname.replacement.value.key}")
    String lastNameReplacementValueKey;

    @Value("${tablename.replacement.value.key}")
    String tableNameReplacementValueKey;

    @Value("${insert.query.replacement.value.key}")
    String insertIntoBackupQueryReplacementValueKey;

    @Value("${interval.days.replacement.value.key}")
    String intervalDaysReplacementValueKey;

    @Value("${internal.source.name.replacement.value.key}")
    String internalSourceNameReplacementValueKey;

    @Value("${date.column.name.replacement.value.key}")
    String dateColumnNameReplacementValueKey;
    
    @Value("${open.replacement.value.key}")
    String openColumnNameReplacementValueKey;
    
    @Value("${click.replacement.value.key}")
    String clickColumnNameReplacementValueKey;
    
    @Value("${open_at.replacement.value.key}")
    String openatColumnNameReplacementValueKey;
    
    
    
    @Value("${extra.where.conditions.replacement.value.key}")
    String extraWhereConditionsReplacementValueKey;

    @Value("${ip.click.open.table.name.value.key}")
    String ipClickOpenTableNameReplacementValueKey;

    @Value("${provider.table.name.value.key}")
    String providerTableNameValueKey;

    @Value("${ftp.export.source.name.value.key}")
    String ftpSourceNameReplacementValueKey;

    @Value("${user.source.name.value.key}")
    String userSourceNameValueKey;

    @Value("${interval.days.append.string.conditions.replacement.value.key}")
    String intervalDaysAppendStringConditionsReplacementValueKey;

    @Value("${advertisement.conditions.replacement.value.key}")
    String advertisementConditionsReplacementValueKey;

    @Autowired
    Map<String, TableStructureModel> tableStructureMap;

    @Autowired
    @Qualifier("tableDomainMap")
    Map<String, String> tableDomainMap;

    @Override
    public List<UserToExportModel> createUserToExportListWithCompleteTableQueries(UserToExportOuterModel userToExportOuterModel, Map<String, List<String>> userSourceTablesMap,
            Integer intervalDaysValue, boolean firstTimeExport, String ftpSourceName) {

        List<UserToExportModel> userToExportModelList = new ArrayList<UserToExportModel>();

        for (UserToExportModel userModel : userToExportOuterModel.getUserToExportModel()) {

            if (userModel.isActive()) {
                try {
                    // get tables
                    Set<String> userSourceTablesList = new HashSet<String>();

                    System.out.println(new Gson().toJson(userSourceTablesMap));

                    // get tables for internalsources/advertisement
                    for (String internalSourceName : userModel.getInternalSourceNames()) {

                        if (userSourceTablesMap.get(internalSourceName.toLowerCase()) != null && userSourceTablesMap.get(internalSourceName.toLowerCase()).size() > 0) {

                            userSourceTablesList.addAll(userSourceTablesMap.get(internalSourceName.toLowerCase()));
                        }
                    }
                    // userSourceTablesMap.get(userModel.getSourceName());

                    for (String userTable : userSourceTablesList) {
                        try {
                            // Parsing it by gson to prevent same reference by objects
                            UserToExportModel userToExportModel = new Gson().fromJson(new Gson().toJson(userModel), UserToExportModel.class);

                            String query = userToExportModel.getQuery() == null || userToExportModel.getQuery().trim().equals("") ? userToExportOuterModel.getQuery() : userToExportModel.getQuery();

                            // Handle advertisement condition and empty advertisement for certain source table combination
                            String adevertisementCondition = "";
                            if (userModel.isAdvertisementRequired()) {
                                adevertisementCondition = userToExportOuterModel.getAdevertisementCondition();
                                String internalSourcesString = "";

                                for (String internalSourceName : userToExportModel.getInternalSourceNames()) {
                                    internalSourcesString = internalSourcesString + "'" + internalSourceName + "'" + ", ";
                                }
                                internalSourcesString = internalSourcesString.replaceAll(", $", "");
                                for (String emptyAdvertisementTable : userModel.getEmptyAdvertisementTables()) {
                                    if (userTable.equals(emptyAdvertisementTable)) {
                                        internalSourcesString = "''";
                                    }
                                }

                                adevertisementCondition = adevertisementCondition.replace(internalSourceNameReplacementValueKey, internalSourcesString);
                            }

                            // Handle intervalDaysCondition and firstTimeExporIntervaltDaysCondition
                            String intervalDaysCondition = userToExportOuterModel.getIntervalDaysCondition();
                            if (firstTimeExport) {
                                intervalDaysCondition = userToExportOuterModel.getFirstTimeExporIntervaltDaysCondition();
                            }

                            String insertIntoBackupQuery = userToExportOuterModel.getInsertIntoBackupQuery();

                            query = query.replace(insertIntoBackupQueryReplacementValueKey, insertIntoBackupQuery);

                            query = query.replace(tableNameReplacementValueKey, userTable);
                            query = query.replace(intervalDaysAppendStringConditionsReplacementValueKey, intervalDaysCondition);
                            query = query.replace(intervalDaysReplacementValueKey, intervalDaysValue.toString());
                            query = query.replace(advertisementConditionsReplacementValueKey, adevertisementCondition);
                            query = query.replace(extraWhereConditionsReplacementValueKey, userToExportModel.getExtraWhereCondition() + " " + userToExportOuterModel.getGlobalExtraWhereConditions());

//                            Update table structure column name
                            query = updateQueryForTableStructureModelKeys(query, userTable);

                            query = query.replace(userSourceNameValueKey, userModel.getSourceName());
                            query = query.replace(ftpSourceNameReplacementValueKey, ftpSourceName);

                            String selectQuery = userToExportOuterModel.getSelectQuery();
                            selectQuery = selectQuery.replace(userSourceNameValueKey, userModel.getSourceName());
                            selectQuery = selectQuery.replace(ftpSourceNameReplacementValueKey, ftpSourceName);

                            userToExportModel.setQuery(query);
                            userToExportModel.setSelectQuery(selectQuery);
                            userToExportModel.setCurrentTable(userTable);
                            userToExportModelList.add(userToExportModel);

                            System.out.println(query);

                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return userToExportModelList;
    }

    @Override
    public String updateQueryForTableStructureModelKeys(String query, String tableName) {

        TableStructureModel tableStructureModel = getTableStructureModel(tableName);

        query = query.replace(firstNameReplacementValueKey, tableStructureModel.getFirstName());
        query = query.replace(lastNameReplacementValueKey, tableStructureModel.getLastName());
        query = query.replace(dateColumnNameReplacementValueKey, tableStructureModel.getDate());
        query = query.replace(openColumnNameReplacementValueKey, tableStructureModel.getOpen());
        query = query.replace(clickColumnNameReplacementValueKey, tableStructureModel.getClick());
        query = query.replace(openatColumnNameReplacementValueKey, tableStructureModel.getOpen_at());      
        
        

        return query;
    }

    @Override
    public TableStructureModel getTableStructureModel(String userTable) {

        return tableStructureMap.get(userTable) != null ? tableStructureMap.get(userTable) : tableStructureMap.get(generalTableStructureKeyName);
    }

    @Override
    @Transactional
    public List<CsvExportModel> getUserFromDatabaseSourceWise(UserToExportModel userToExportModel, Integer start, Integer limit) {

        // userToExportModel.setQuery(userToExportModel.getQuery() + " limit " + start + ", " + limit);

        String query = userToExportModel.getSelectQuery() + " limit " + start + ", " + limit;

        List<CsvExportModel> result = userToExportModelRepository.getUserFromDatabaseSourceWise(query);

        for (CsvExportModel csvModel : result) {
            String keyword = csvModel.getKeyword();

            if (keyword.contains("\\") || keyword.contains("#")) {
                keyword = keyword.replace("\\", " ").replace("#", " ");
                csvModel.setKeyword(keyword);
            }

            csvModel.setDomain(tableDomainMap.get(userToExportModel.getCurrentTable()));

        }

        return result;
    }

    @Override
    public List<CsvExportModel> updateIpAddressFromEmailIpMap(List<CsvExportModel> csvExportModelList, Map<String, String> emailIpModelMap) {

        for (CsvExportModel csvExportModel : csvExportModelList) {

            try {
                csvExportModel.setIp(emailIpModelMap.get(csvExportModel.getEmail()));

                if (csvExportModel.getIp() == null ||csvExportModel.getIp().equals("")) {
                    csvExportModel.setIp("52.6.190.27");
                }

            } catch (Exception e) {
                // Handling for null IP
                csvExportModel.setIp("52.6.190.27");
            }
        }
        return csvExportModelList;
    }

    @Override
    public Map<String, String> getEmailIpMap(IpTableMapModel ipTableMapModel, UserToExportModel userToExportModel, FtpExportSourcesModel ftpExportSourcesModel) {

        Map<String, String> emailIpModelMap = new HashMap<String, String>();
        List<String> tableWithIpList = ipTableMapModel.getTables();
        String ipLogTableQuery = ipTableMapModel.getIpClickOpenTableQuery();

        for (String ipLogTable : tableWithIpList) {

            List<CsvExportModel> csvExportModelListWithIp = getIpAddressFromClickLogTable(ipLogTable, userToExportModel.getSourceName(), ftpExportSourcesModel.getSourceName(), ipLogTableQuery);
            for (CsvExportModel csvExportModel : csvExportModelListWithIp) {

                emailIpModelMap.put(csvExportModel.getEmail(), csvExportModel.getIp());
            }
        }

        return emailIpModelMap;
    }

    @Override
    @Transactional
    public List<CsvExportModel> getIpAddressFromClickLogTable(String ipLogTable, String userSource, String ftpSource, String ipLogTableQuery) {

        String query = ipLogTableQuery;

        // query = updateQueryForTableStructureModelKeys(query, providerTable);

        query = query.replace(ipClickOpenTableNameReplacementValueKey, ipLogTable);
        query = query.replace(userSourceNameValueKey, userSource);
        query = query.replace(ftpSourceNameReplacementValueKey, ftpSource);
        // query = query.replace(intervalDaysReplacementValueKey, intervalDays.toString());

        List<CsvExportModel> result = userToExportModelRepository.getUserFromDatabaseSourceWise(query);

        return result;
    }

    @Override
    public void insertIntoSeekerBackupTableService(List<UserToExportModel> userExportModelFinalWithQueries) {

        for (UserToExportModel exportModel : userExportModelFinalWithQueries) {
            userToExportModelRepository.insertIntoSeekerBackupTable(exportModel);
        }
    }

    @Override
    public void deleteSeekerExportByIdList(List<CsvExportModel> csvExportModelList) {

        String deleteIds = "";
        for (CsvExportModel cs : csvExportModelList) {
            deleteIds = deleteIds + ", " + cs.getId();
        }
        deleteIds = StringUtils.replaceOnce(deleteIds, ",", "");
        String deleteQuery = "delete from tbl_user_export_backup where id IN (" + deleteIds + ") ";

        userToExportModelRepository.deleteSeekerExportByIdList(deleteQuery);

    }

    // // TODO CRETE DELETE QUERIES HERE AND CALL REPO

    // @Override
    // @Transactional
    // public Map<String, String> createTableDomainMapService() {
    //
    // Map<String, String> tableDomainMap = userToExportModelRepository.createTableDomainMapRepository();
    //
    // tableDomainMap.put("tbl_job_seekers_paperrosejobs", "paperrosejobs.com");
    //
    // return tableDomainMap;
    // }
}
