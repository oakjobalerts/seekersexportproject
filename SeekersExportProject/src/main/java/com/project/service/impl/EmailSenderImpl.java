package com.project.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.project.model.EmailReceiver;
import com.project.model.StatsOuterModel;
import com.project.service.EmailSender;

@Service
public class EmailSenderImpl implements EmailSender {

    @Value("${mail.smtp.user}")
    String user;

    @Value("${mail.smtp.password}")
    String pass;

    @Value("${mail.smtp.host}")
    String mailSmtpHost;

    @Value("${mail.transport.protocol}")
    String mailTransportProtocol;

    @Value("${mail.smtp.port}")
    Integer mailSmtpPort;

    @Value("${mail.smtp.auth}")
    String mailSmtpAuth;

    @Value("${mail.smtp.socketFactory.port}")
    Integer mailSmtpSocketFactoryPort;

    @Value("${mail.smtp.socketFactory.class}")
    String mailSmtpSocketFactoryClass;

    @Override
    public String sendSmtpEmail(String subject, String body, EmailReceiver emailReceiver) {

        Properties props = System.getProperties();

        props.put("mail.smtp.host", mailSmtpHost);
        props.put("mail.transport.protocol", mailTransportProtocol);
        props.put("mail.smtp.port", mailSmtpPort);
        props.put("mail.smtp.auth", mailSmtpAuth);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.socketFactory.port", mailSmtpSocketFactoryPort);
        props.put("mail.smtp.socketFactory.class", mailSmtpSocketFactoryClass);

        Authenticator auth = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }
        };

        Session session = Session.getInstance(props, auth);

        try {
            MimeMessage msg = new MimeMessage(session);

            for (String cc : emailReceiver.getCcList()) {
                msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
            }

            for (String bcc : emailReceiver.getBccList()) {
                msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
            }

            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");
            msg.setSubject(subject, "UTF-8");
            msg.setContent(body, "text/html");

            msg.setFrom(new InternetAddress(user, "FBG Reports"));
            msg.setReplyTo(InternetAddress.parse(user, false));

            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailReceiver.getTo(), false));
            Transport.send(msg);

            System.out.println("Email Sent Successfully!!");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Email Sent Successfully!!";
    }

    @Override
    public String createEmailBody(StatsOuterModel statsOuterModel, Integer maxLimit) {

        String maxMonthlyCountLabel = "Max Monthly count : " + maxLimit;
        if (maxLimit == 0) {
            maxMonthlyCountLabel = "No max Monthly limit for " + statsOuterModel.getExportToSourceName();
        }

        String body = "";

        body = body + "<div>"
                + maxMonthlyCountLabel
                + "</div>"
                + "<br/>";

        body = body + "<div>" + "<div>"
                + "<label>Total sent this month</label>"
                + "<br/>"
                + "<table cellspacing='0' cellpadding='12' border='0' width='900'>"
                + "<thead>"
                + "<tr>"
                + "<th style='border-bottom: 2px solid #C35D5D;'>Total</th>";

        for (String key : statsOuterModel.getStatsModelMap().keySet()) {
            body = body
                    + "<th style='border-bottom: 2px solid #C35D5D;'>" + key + "</th>";
        }
        body = body + "</tr>" + "</thead>";

        body = body + "<tbody style='text-align: center;'>";
        body = body + "<tr>" + "<td style='border-bottom: 1px solid black;'>" + statsOuterModel.getTotalSent() + "</td>";

        for (String key : statsOuterModel.getStatsModelMap().keySet()) {
            body = body
                    + "<td style='border-bottom: 1px solid black;'>" + statsOuterModel.getStatsModelMap().get(key).getSubUserTotalSent() + "</td>";
        }
        body = body + "</tr>" + "</<tbody>" + "</table>" + "</div>";
        body = body + "<br/>" + "<br/>";

        body = body + "<div>"
                + "<label>Total Opener/Clickers Count : " + statsOuterModel.getTotalSent() + "</label>"
                + "<table cellspacing='0' cellpadding='12' border='0' width='900'>"
                + "<thead>" + "<tr>"
                + "<th style='border-bottom: 2px solid #C35D5D;'>Date</th>";

        for (String key : statsOuterModel.getStatsModelMap().keySet()) {
            body = body
                    + "<th style='border-bottom: 2px solid #C35D5D;'>" + key + "</th>";
        }

        body = body + "</tr>" + "</thead>";

        body = body + "<tbody style='text-align: center;'>";

        List<String> dateKeyList = new ArrayList<String>();
        for (String key : statsOuterModel.getTotalSentDateWise().keySet()) {
            dateKeyList.add(key);
        }

        for (int i = dateKeyList.size() - 1; i >= 0; i--) {
            body = body + "<tr>"
                    + "<td style='border-bottom: 1px solid black;'>" + dateKeyList.get(i) + "</td>";

            for (String key : statsOuterModel.getStatsModelMap().keySet()) {
                body = body
                        + "<td style='border-bottom: 1px solid black;'>" + statsOuterModel.getStatsModelMap().get(key).getDateWiseCount().get(dateKeyList.get(i)) + "</td>";
            }
            body = body + "</tr>";
        }

        body = body + "</tbody>" + "</table>" + "</div>" + "</div>";

        return body;
    }
}
