package com.project.service;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.client.HttpClient;

import com.opencsv.CSVWriter;
import com.project.model.CsvExportModel;
import com.project.model.FtpExportSourcesModel;
import com.sun.jersey.api.client.WebResource;

public interface FtpExportSourcesModelService {

    FTPClient createFtpConnection(String host, String user, String pass, int port);

    CSVWriter createCsvWriter(FtpExportSourcesModel exportSourcesModel, String folderPath, String fileName);

    void createCsvFileLineByLine(CsvExportModel csvExportModel, CSVWriter csvWriter, String ftpsource);

    void closeCsvWriter(CSVWriter csvWriter);

    CsvExportModel writeToFtpExportThroughApi(CsvExportModel csvExportModel, FtpExportSourcesModel exportSourcesModel, HttpClient httpClient);

    void uploadFileOverFtpServer(FtpExportSourcesModel exportSourcesModel, String folderPath, String fileName);

    CsvExportModel ZrExportApi(CsvExportModel csvExportModel, FtpExportSourcesModel exportSourcesModel, WebResource webResource);

}
