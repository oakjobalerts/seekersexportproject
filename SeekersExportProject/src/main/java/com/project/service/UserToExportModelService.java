package com.project.service;

import java.util.List;
import java.util.Map;

import com.project.model.CsvExportModel;
import com.project.model.FtpExportSourcesModel;
import com.project.model.IpTableMapModel;
import com.project.model.TableStructureModel;
import com.project.model.UserToExportModel;
import com.project.model.UserToExportOuterModel;

public interface UserToExportModelService {

    List<UserToExportModel> createUserToExportListWithCompleteTableQueries(UserToExportOuterModel userToExportOuterModel,
            Map<String, List<String>> userSourceTablesMap, Integer intervalDaysValue, boolean firstTimeExport, String ftpSourceName);

    List<CsvExportModel> getUserFromDatabaseSourceWise(UserToExportModel userToExportModel, Integer start, Integer limit);

    TableStructureModel getTableStructureModel(String userTable);

    List<CsvExportModel> getIpAddressFromClickLogTable(String ipLogTable, String userSource, String ftpSource, String ipLogTableQuery);

    Map<String, String> getEmailIpMap(IpTableMapModel ipTableMapModel, UserToExportModel userToExportModel, FtpExportSourcesModel ftpExportSourcesModel);

    List<CsvExportModel> updateIpAddressFromEmailIpMap(List<CsvExportModel> csvExportModelList, Map<String, String> emailIpModelMap);

    String updateQueryForTableStructureModelKeys(String query, String tableName);

    void insertIntoSeekerBackupTableService(List<UserToExportModel> userExportModelFinalWithQueries);

	void deleteSeekerExportByIdList(List<CsvExportModel> csvExportModelList);

    // Map<String, String> createTableDomainMapService();

}
