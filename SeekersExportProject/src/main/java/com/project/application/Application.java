package com.project.application;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.project.controller.HomeController;

@Configuration
@ComponentScan("com.project")
@PropertySources({
        @PropertySource("classpath:/application.properties"),
        @PropertySource("classpath:/smtp.properties") })
public class Application {

    public static void main(String[] args) {
    	System.out.println("start");
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        HomeController homeController = context.getBean(HomeController.class);
        homeController.startProcess();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
