package com.project.repository;

import java.util.List;

import com.project.model.FtpExportSourcesModel;

public interface FtpExportSourcesModelRepository {

    List<FtpExportSourcesModel> createExportSeekersModelList(String exportSeekersFilePath);

}
