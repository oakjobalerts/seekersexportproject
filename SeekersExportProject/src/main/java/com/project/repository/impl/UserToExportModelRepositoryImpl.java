package com.project.repository.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.project.model.AdvertesementTableMappingModel;
import com.project.model.CsvExportModel;
import com.project.model.IpTableMapModel;
import com.project.model.TableStructureModel;
import com.project.model.UserToExportModel;
import com.project.model.UserToExportOuterModel;
import com.project.repository.UserToExportModelRepository;

@Repository
public class UserToExportModelRepositoryImpl implements UserToExportModelRepository {

    @Autowired
    SessionFactory sessionFactory;

    @Value("${tables.to.skip.string}")
    String tablesToSkipString;

    @Override
    public Map<String, List<String>> getSourceNameTableListMap(String sourceNameTableFilePath) {

        Type type = new TypeToken<Map<String, List<String>>>() {
        }.getType();

        Map<String, List<String>> sourceNameTableMap = new HashMap<String, List<String>>();
        try {
            FileReader fileReader = new FileReader(sourceNameTableFilePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            sourceNameTableMap = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, type);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sourceNameTableMap;
    }

    @Override
    public Map<String, List<String>> getSourceNameTableListMapDynamicallyFromSeekerImportJason(String advertisementTableMapping) {

        Type type = new TypeToken<List<AdvertesementTableMappingModel>>() {
        }.getType();

        List<AdvertesementTableMappingModel> advertisementModel = new ArrayList<AdvertesementTableMappingModel>();

        Map<String, List<String>> sourceNameTableMap = new HashMap<String, List<String>>();

        try {
            FileReader fileReader = new FileReader(advertisementTableMapping);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            advertisementModel = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, type);

            for (Entry<String, String> map : advertisementModel.get(0).getAdvertisement_map().entrySet()) {

                // try {
                // Integer.parseInt(map.getKey());
                // continue;
                // } catch (Exception e) {
                // System.out.println("exception : " + map.getKey());
                // }

                String[] tablsArray = map.getValue().split("\\|");
                List<String> initialTableListWithAllTables = Arrays.asList(tablsArray);

                // Remove cylcon and other tables where we move openers and clickers to remove duplicate users
                List<String> tablesList = new ArrayList<String>();
                for (String table : initialTableListWithAllTables) {

                    if (!tablesToSkipString.toLowerCase().contains(table.toLowerCase().trim())) {
                        tablesList.add(table);
                    }
                }

                sourceNameTableMap.put(map.getKey().toLowerCase(), tablesList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(new Gson().toJson(sourceNameTableMap));

        return sourceNameTableMap;
    }

    @Override
    public Map<String, TableStructureModel> getTableStructureMap(String tableStrucreFilePath) {

        Type type = new TypeToken<Map<String, TableStructureModel>>() {
        }.getType();

        Map<String, TableStructureModel> tableStructureModelMap = new HashMap<String, TableStructureModel>();
        try {
            FileReader fileReader = new FileReader(tableStrucreFilePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            tableStructureModelMap = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, type);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tableStructureModelMap;
    }

    @Override
    public UserToExportOuterModel getUserExportOuterModel(String userExportFilePath) {

        UserToExportOuterModel userToExportOuterModel = new UserToExportOuterModel();
        try {
            FileReader fileReader = new FileReader(userExportFilePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            userToExportOuterModel = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, UserToExportOuterModel.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return userToExportOuterModel;

    }

    @Override
    public IpTableMapModel getIpTableMapModel(String ipTableMapModelFilePath) {

        IpTableMapModel ipTableMapModel = new IpTableMapModel();
        try {
            FileReader fileReader = new FileReader(ipTableMapModelFilePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            ipTableMapModel = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, IpTableMapModel.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ipTableMapModel;

    }

    @Override
    @Transactional
    public List<CsvExportModel> getUserFromDatabaseSourceWise(String query) {

        try {
            Query sqlQuery = getSessionFactory().createSQLQuery(query)
                    .setResultTransformer(Transformers.aliasToBean(CsvExportModel.class));

            List<CsvExportModel> result = sqlQuery.list();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<CsvExportModel>();
    }

    public Session getSessionFactory() {
        return sessionFactory.getCurrentSession();
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public Map<String, String> createTableDomainMapRepository() {

        Map<String, String> tableDomainMap = new HashMap<String, String>();

        String sqlQueryString = "select  table_name as tableName, website_url as websiteUrl"
                + " from whitelabels_process "
                + " where EmailClient='sendgrid' OR EmailClient='sparkpost' OR EmailClient='mailgun' group by DomainUrl ";

        SQLQuery sqlQuery = getSessionFactory().createSQLQuery(sqlQueryString);

        List<Object[]> rows = sqlQuery.list();

        for (Object[] row : rows) {

            String tables[] = row[0].toString().split("\\|");
            for (int i = 0; i < tables.length; i++) {
                tableDomainMap.put(tables[i], row[1].toString());
            }
            // tableDomainMap.put(row[0].toString(), row[1].toString());
        }

        String sqlQueryString2 = "select table_name as tableName, website_url as websiteUrl"
                + " from whitelabels_process "
                + " INNER JOIN tbl_domain_usertype_mapping "
                + " ON whitelabels_process.DomainUrl=tbl_domain_usertype_mapping.domain "
                + " WHERE tbl_domain_usertype_mapping.usertype='o/c' ";

        SQLQuery sqlQuery2 = getSessionFactory().createSQLQuery(sqlQueryString2);
        List<Object[]> rows2 = sqlQuery2.list();

        for (Object[] row : rows2) {
            String tables[] = row[0].toString().split("\\|");
            for (int i = 0; i < tables.length; i++) {
                tableDomainMap.put(tables[i], row[1].toString());
            }

        }

        System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(tableDomainMap));

        return tableDomainMap;
    }

    @Transactional
    @Override
    public void insertIntoSeekerBackupTable(UserToExportModel exportModel) {

        getSessionFactory().createSQLQuery(exportModel.getQuery()).executeUpdate();

    }

    @Override
    @Transactional
    public void deleteSeekerExportByIdList(String deleteQuery) {

        getSessionFactory().createSQLQuery(deleteQuery).executeUpdate();

    }
}
