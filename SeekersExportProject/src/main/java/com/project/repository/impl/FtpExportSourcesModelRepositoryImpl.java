package com.project.repository.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.project.model.FtpExportSourcesModel;
import com.project.repository.FtpExportSourcesModelRepository;

@Repository
public class FtpExportSourcesModelRepositoryImpl implements FtpExportSourcesModelRepository {

    @Override
    public List<FtpExportSourcesModel> createExportSeekersModelList(String exportSeekersFilePath) {

        Type type = new TypeToken<List<FtpExportSourcesModel>>() {
        }.getType();

        List<FtpExportSourcesModel> ftpExportSourceList = new ArrayList<FtpExportSourcesModel>();

        try {
            FileReader fileReader = new FileReader(exportSeekersFilePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            ftpExportSourceList = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, type);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ftpExportSourceList;
    }

}
