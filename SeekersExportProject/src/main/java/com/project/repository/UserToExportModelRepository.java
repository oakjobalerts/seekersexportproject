package com.project.repository;

import java.util.List;
import java.util.Map;

import com.project.model.CsvExportModel;
import com.project.model.IpTableMapModel;
import com.project.model.TableStructureModel;
import com.project.model.UserToExportModel;
import com.project.model.UserToExportOuterModel;

public interface UserToExportModelRepository {

    // autowire
    Map<String, List<String>> getSourceNameTableListMap(String sourceNameTableFilePath);

    // autowire
    Map<String, TableStructureModel> getTableStructureMap(String tableStrucreFilePath);

    // autowire
    UserToExportOuterModel getUserExportOuterModel(String userExportFilePath);

    List<CsvExportModel> getUserFromDatabaseSourceWise(String query);

    IpTableMapModel getIpTableMapModel(String ipTableMapModelFilePath);

    Map<String, String> createTableDomainMapRepository();

    void insertIntoSeekerBackupTable(UserToExportModel exportModel);

    void deleteSeekerExportByIdList(String deleteQuery);

    Map<String, List<String>> getSourceNameTableListMapDynamicallyFromSeekerImportJason(String advertisementTableMapping);
}
