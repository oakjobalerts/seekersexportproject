package com.project.model;

import java.util.List;

public class FtpExportSourcesModel {

    String sourceName;
    Integer interval;
    String ftpName;
    String ftpUser;
    String ftpPassword;
    Integer maxMonthlyCount;
	Integer dailyLimit;
    boolean active;
    boolean isUrlExport;
    boolean isIpRequired;
    boolean isFirstTimeExport;
    String baseUrl;
    String urlParameters;

    String authKey;

    String trackingName;

    List<String> countries;
    List<String> emailToList;
    List<String> emailBccList;

	public Integer getDailyLimit() {
		return dailyLimit;
	}

	public void setDailyLimit(Integer dailyLimit) {
		this.dailyLimit = dailyLimit;
	}

    public String getTrackingName() {
        return trackingName;
    }

    public void setTrackingName(String trackingName) {
        this.trackingName = trackingName;
    }

    public boolean isIpRequired() {
        return isIpRequired;
    }

    public void setIpRequired(boolean isIpRequired) {
        this.isIpRequired = isIpRequired;
    }

    public boolean isUrlExport() {
        return isUrlExport;
    }

    public void setUrlExport(boolean isUrlExport) {
        this.isUrlExport = isUrlExport;
    }

    public boolean isFirstTimeExport() {
        return isFirstTimeExport;
    }

    public void setFirstTimeExport(boolean isFirstTimeExport) {
        this.isFirstTimeExport = isFirstTimeExport;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUrlParameters() {
        return urlParameters;
    }

    public void setUrlParameters(String urlParameters) {
        this.urlParameters = urlParameters;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getFtpName() {
        return ftpName;
    }

    public String getFtpUser() {
        return ftpUser;
    }

    public String getFtpPassword() {
        return ftpPassword;
    }

    public boolean isActive() {
        return active;
    }

    public List<String> getCountries() {
        return countries;
    }

    public List<String> getEmailToList() {
        return emailToList;
    }

    public List<String> getEmailBccList() {
        return emailBccList;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public void setFtpName(String ftpName) {
        this.ftpName = ftpName;
    }

    public void setFtpUser(String ftpUser) {
        this.ftpUser = ftpUser;
    }

    public void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }

    public void setEmailToList(List<String> emailToList) {
        this.emailToList = emailToList;
    }

    public void setEmailBccList(List<String> emailBccList) {
        this.emailBccList = emailBccList;
    }

    public Integer getMaxMonthlyCount() {
        return maxMonthlyCount;
    }

    public void setMaxMonthlyCount(Integer maxMonthlyCount) {
        this.maxMonthlyCount = maxMonthlyCount;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public FtpExportSourcesModel() {
        super();
    }

}
