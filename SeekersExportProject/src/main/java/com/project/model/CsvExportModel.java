package com.project.model;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CsvExportModel {

    // Name, Email, Location, Date, Timestamp, Keyword, IP, whitelabel
    BigInteger id;
    String name;
    String email;
    String location;
    String regDate;
    String timestamp;
    String keyword;
    String ip;
    String whitelabel;
    String zipcode;

    String firstName;
    String lastName;
    String city;
    String state;
    String openAtTimestamp;

    String domain;
    String response;

    String ftpExportSource;
    String userSource;

    // select email as email, first_name as firstName, last_name as lastName,
    // zipcode as zipcode, city as city, state as state, date as regDate,
    // keyword as keyword, open_at as openAtTimestamp from tbl_job_seekers_diamond
    // where date(date) >= date(NOW() - INTERVAL 90 DAY) and advertisement = 'D4'
    // order by id desc limit 1, 1000

    public String getFtpExportSource() {
        return ftpExportSource;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getUserSource() {
        return userSource;
    }

    public void setFtpExportSource(String ftpExportSource) {
        this.ftpExportSource = ftpExportSource;
    }

    public void setUserSource(String userSource) {
        this.userSource = userSource;
    }

    public String[] toArray() {

        name = firstName + " " + lastName;
        name = name.trim();
        name = name.replace(" ", ", ");

        location = setUpLocation(city, state);

        response = response == null || response.equals("") ? "" : response;

        String[] array = { name, email, location, keyword, zipcode, ip == null || ip.equals("") ? "" : ip, response };
        return array;
    }

    public String[] toArrayRm() {

        name = firstName + " " + lastName;
        name = name.trim();
        name = name.replace(" ", ", ");

        location = setUpLocation(city, state);

        response = response == null || response.equals("") ? "" : response;

        SimpleDateFormat format = new SimpleDateFormat("YYYY-mm-dd");

        String[] array = { name, email, location, keyword, zipcode, ip == null || ip.equals("") ? "52.6.190.27" : ip, domain, format.format(new Date()) };
        return array;
    }

    // "Name", "Email", "Location", "Keyword", "Zipcode", "Ip Address", "Url", "Date Time"
    public String setUpLocation(String city, String state) {

        String location = "";

        if (city != null) {
            city = city.replace(",", " ").trim();
        }
        if (state != null) {
            state = state.replace(",", " ").trim();
        }
        location = city + " " + state;
        location = location.trim();
        // location = location.replace(" ", ", ");

        return location;
    }

    public String[] toCustomJJArray() {

        String customZip = zipcode;
        if (zipcode == null || zipcode.equals("")) {
            location = setUpLocation(city, state);
            customZip = location;
        }

        String[] array = { email, keyword, customZip };

        return array;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getWhitelabel() {
        return whitelabel;
    }

    public void setWhitelabel(String whitelabel) {
        this.whitelabel = whitelabel;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {

        this.zipcode = zipcode;
        if (zipcode != null) {
            this.zipcode = zipcode.split("-")[0];
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRegDate() {
        return regDate;
    }

    public String getOpenAtTimestamp() {
        return openAtTimestamp;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public void setOpenAtTimestamp(String openAtTimestamp) {
        this.openAtTimestamp = openAtTimestamp;
    }

    public CsvExportModel() {
        super();
    }

}
