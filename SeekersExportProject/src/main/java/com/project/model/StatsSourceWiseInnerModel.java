package com.project.model;

import java.util.Map;

public class StatsSourceWiseInnerModel {

    int subUserTotalSent;

    Map<String, Integer> dateWiseCount;

    public int getSubUserTotalSent() {
        return subUserTotalSent;
    }

    public void setSubUserTotalSent(int subUserTotalSent) {
        this.subUserTotalSent = subUserTotalSent;
    }

    public Map<String, Integer> getDateWiseCount() {
        return dateWiseCount;
    }

    public void setDateWiseCount(Map<String, Integer> dateWiseCount) {
        this.dateWiseCount = dateWiseCount;
    }

}
// "sourceName": "Deck Inc",
// "subUserTotalSent": 90,
// "dateWiseCount": {
// "2017-03-16": 40,
// "2017-03-17": 50