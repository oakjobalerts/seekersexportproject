package com.project.model;

public class TableStructureModel {

	String firstName;
	String lastName;
	String date;
	String open;
	String click;
	String open_at;
	
	

	public String getOpen_at() {
		return open_at;
	}

	public void setOpen_at(String open_at) {
		this.open_at = open_at;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getClick() {
		return click;
	}

	public void setClick(String click) {
		this.click = click;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public TableStructureModel() {
		super();
	}

}
