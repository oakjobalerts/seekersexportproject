package com.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_user_export_backup")
public class SeekerExportBackup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "bigint(11)")
    private long id;

    String name;
    String email;
    String location;
    String regDate;
    String timestamp;
    String keyword;
    String ip;
    String whitelabel;
    String zipcode;

    String firstName;
    String lastName;
    String city;
    String state;
    String openAtTimestamp;

    String domain;
    String response;

    String ftpExportSource;
    String userSource;

    public String getFtpExportSource() {
        return ftpExportSource;
    }

    public String getUserSource() {
        return userSource;
    }

    public void setFtpExportSource(String ftpExportSource) {
        this.ftpExportSource = ftpExportSource;
    }

    public void setUserSource(String userSource) {
        this.userSource = userSource;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getLocation() {
        return location;
    }

    public String getRegDate() {
        return regDate;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getIp() {
        return ip;
    }

    public String getWhitelabel() {
        return whitelabel;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getOpenAtTimestamp() {
        return openAtTimestamp;
    }

    public String getDomain() {
        return domain;
    }

    public String getResponse() {
        return response;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setWhitelabel(String whitelabel) {
        this.whitelabel = whitelabel;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setOpenAtTimestamp(String openAtTimestamp) {
        this.openAtTimestamp = openAtTimestamp;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public SeekerExportBackup() {
        super();
    }

}
