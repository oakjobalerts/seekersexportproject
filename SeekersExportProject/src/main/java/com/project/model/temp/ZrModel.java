package com.project.model.temp;

public class ZrModel {

    String email;
    String create_time;
    String search;
    String location;

    public String getEmail() {
        return email;
    }

    public String getCreate_time() {
        return create_time;
    }

    public String getSearch() {
        return search;
    }

    public String getLocation() {
        return location;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
