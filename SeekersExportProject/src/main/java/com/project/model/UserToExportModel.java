package com.project.model;

import java.util.List;

public class UserToExportModel {

    String sourceName;
    String query;
    String extraWhereCondition;
    String currentTable;
    boolean active;
    boolean advertisementRequired;
    List<String> internalSourceNames;
    List<String> emptyAdvertisementTables;

    String selectQuery;

    public String getSelectQuery() {
        return selectQuery;
    }

    public void setSelectQuery(String selectQuery) {
        this.selectQuery = selectQuery;
    }

    public boolean isAdvertisementRequired() {
        return advertisementRequired;
    }

    public void setAdvertisementRequired(boolean advertisementRequired) {
        this.advertisementRequired = advertisementRequired;
    }

    public String getCurrentTable() {
        return currentTable;
    }

    public void setCurrentTable(String currentTable) {
        this.currentTable = currentTable;
    }

    // List<String> tables;
    public String getSourceName() {
        return sourceName;
    }

    public String getQuery() {
        return query;
    }

    public String getExtraWhereCondition() {
        return extraWhereCondition;
    }

    public boolean isActive() {
        return active;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setExtraWhereCondition(String extraWhereCondition) {
        this.extraWhereCondition = extraWhereCondition;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<String> getInternalSourceNames() {
        return internalSourceNames;
    }

    public void setInternalSourceNames(List<String> internalSourceNames) {
        this.internalSourceNames = internalSourceNames;
    }

    public List<String> getEmptyAdvertisementTables() {
        return emptyAdvertisementTables;
    }

    public void setEmptyAdvertisementTables(List<String> emptyAdvertisementTables) {
        this.emptyAdvertisementTables = emptyAdvertisementTables;
    }

}
