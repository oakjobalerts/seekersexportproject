package com.project.model;

import java.util.Map;

public class StatsOuterModel {

    String date;
    String exportToSourceName;
    int totalSent;
    String maxLimitReachedDate;
    int totalCountAfterMaxLimit;

    Map<String, Integer> sourceWiseCountAfterMaxLimit;
    Map<String, Integer> totalSentDateWise;
    Map<String, StatsSourceWiseInnerModel> statsModelMap;

    public String getDate() {
        return date;
    }

    public String getExportToSourceName() {
        return exportToSourceName;
    }

    public int getTotalSent() {
        return totalSent;
    }

    public String getMaxLimitReachedDate() {
        return maxLimitReachedDate;
    }

    public int getTotalCountAfterMaxLimit() {
        return totalCountAfterMaxLimit;
    }

    public void setTotalCountAfterMaxLimit(int totalCountAfterMaxLimit) {
        this.totalCountAfterMaxLimit = totalCountAfterMaxLimit;
    }

    public Map<String, Integer> getSourceWiseCountAfterMaxLimit() {
        return sourceWiseCountAfterMaxLimit;
    }

    public Map<String, Integer> getTotalSentDateWise() {
        return totalSentDateWise;
    }

    public Map<String, StatsSourceWiseInnerModel> getStatsModelMap() {
        return statsModelMap;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setExportToSourceName(String exportToSourceName) {
        this.exportToSourceName = exportToSourceName;
    }

    public void setTotalSent(int totalSent) {
        this.totalSent = totalSent;
    }

    public void setMaxLimitReachedDate(String maxLimitReachedDate) {
        this.maxLimitReachedDate = maxLimitReachedDate;
    }

    public void setSourceWiseCountAfterMaxLimit(Map<String, Integer> sourceWiseCountAfterMaxLimit) {
        this.sourceWiseCountAfterMaxLimit = sourceWiseCountAfterMaxLimit;
    }

    public void setTotalSentDateWise(Map<String, Integer> totalSentDateWise) {
        this.totalSentDateWise = totalSentDateWise;
    }

    public void setStatsModelMap(Map<String, StatsSourceWiseInnerModel> statsModelMap) {
        this.statsModelMap = statsModelMap;
    }

}
