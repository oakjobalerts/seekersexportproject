package com.project.model;

import java.util.List;

public class IpTableMapModel {

    String ipClickOpenTableQuery;
    List<String> tables;

    public String getIpClickOpenTableQuery() {
        return ipClickOpenTableQuery;
    }

    public List<String> getTables() {
        return tables;
    }

    public void setIpClickOpenTableQuery(String ipClickOpenTableQuery) {
        this.ipClickOpenTableQuery = ipClickOpenTableQuery;
    }

    public void setTables(List<String> tables) {
        this.tables = tables;
    }

    public IpTableMapModel() {
        super();
    }

}
