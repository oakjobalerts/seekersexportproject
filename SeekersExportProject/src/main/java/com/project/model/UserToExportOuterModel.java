package com.project.model;

import java.util.List;

public class UserToExportOuterModel {

    String query;
    String globalExtraWhereConditions;
    String adevertisementCondition;
    String intervalDaysCondition;
    String firstTimeExporIntervaltDaysCondition;

    List<UserToExportModel> userToExportModel;

    String insertIntoBackupQuery;
    String selectQuery;

    public String getSelectQuery() {
        return selectQuery;
    }

    public void setSelectQuery(String selectQuery) {
        this.selectQuery = selectQuery;
    }

    public String getInsertIntoBackupQuery() {
        return insertIntoBackupQuery;
    }

    public void setInsertIntoBackupQuery(String insertIntoBackupQuery) {
        this.insertIntoBackupQuery = insertIntoBackupQuery;
    }

    public String getGlobalExtraWhereConditions() {
        return globalExtraWhereConditions;
    }

    public void setGlobalExtraWhereConditions(String globalExtraWhereConditions) {
        this.globalExtraWhereConditions = globalExtraWhereConditions;
    }

    public String getQuery() {
        return query;
    }

    public List<UserToExportModel> getUserToExportModel() {
        return userToExportModel;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setUserToExportModel(List<UserToExportModel> userToExportModel) {
        this.userToExportModel = userToExportModel;
    }

    public String getAdevertisementCondition() {
        return adevertisementCondition;
    }

    public void setAdevertisementCondition(String adevertisementCondition) {
        this.adevertisementCondition = adevertisementCondition;
    }

    public String getIntervalDaysCondition() {
        return intervalDaysCondition;
    }

    public String getFirstTimeExporIntervaltDaysCondition() {
        return firstTimeExporIntervaltDaysCondition;
    }

    public void setIntervalDaysCondition(String intervalDaysCondition) {
        this.intervalDaysCondition = intervalDaysCondition;
    }

    public void setFirstTimeExporIntervaltDaysCondition(String firstTimeExporIntervaltDaysCondition) {
        this.firstTimeExporIntervaltDaysCondition = firstTimeExporIntervaltDaysCondition;
    }

}
