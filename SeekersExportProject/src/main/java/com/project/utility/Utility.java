package com.project.utility;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

@Controller
public class Utility {

    @Value("${server.folder.path}")
    String server27FolderPath;

    static DateFormat dateFormatter = new SimpleDateFormat("YYYY-MM-dd");

    public String getTodayDate() {

        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormatter.format(new Date());
    }

    public String nullCheck(String value) {

        return value == null ? "" : value;
    }

    public String getCurretMonth() {

        DateFormat dateFormatter = new SimpleDateFormat("MMMM");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormatter.format(new Date());

    }
    
    public String get48hrsErlierDate() {
    	
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -2);        
        return dateFormat.format(cal.getTime());
        
    }

    public String getCurretYear() {

        DateFormat dateFormatter = new SimpleDateFormat("YYYY");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormatter.format(new Date());
    }

    public String getDateMonthINNumber() {

        DateFormat dateFormatter = new SimpleDateFormat("ddMM");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormatter.format(new Date());
    }

    public String getDateByInterval(int interval) {

        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, interval);

        return dateFormatter.format(calendar.getTime());
    }

    public String convertDateFormatForL5Export(String date) {

        String l5Date = "";

        DateFormat alertDateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        Date alertDate;

        try {
            alertDate = alertDateFormatter.parse(date);
            DateFormat l5DateFormatter = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
            l5Date = l5DateFormatter.format(alertDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return l5Date;
    }

    public String getLastMonthYear() {

        DateFormat dateFormatter = new SimpleDateFormat("MMMM-YYYY");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -1);

        return dateFormatter.format(calendar.getTime());
    }

    public boolean isStartDateSmallerOrEqualsThanEndDate(String startDate, String endDate) {

        try {

            startDate = startDate.split(" ")[0];
            endDate = endDate.split(" ")[0];

            Date start = dateFormatter.parse(startDate);
            Date end = dateFormatter.parse(endDate);

            if (start.getTime() <= end.getTime()) {

                return true;
            }

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return false;
    }

    public String getcsvFileNameFromExportSource(String exportSourceName) {

        String fileName = File.separator + "firebrickgroup_" + exportSourceName + "_" + getTodayDate() + ".csv";
        return fileName;
    }

    public String getcsvFileNameForMaxMonthlyLimitBackupFile(String exportSourceName) {

        String fileName = File.separator + "firebrickgroup_" + exportSourceName + "_" + getCurretMonth() + "-" + getCurretYear() + "_backup" + ".csv";
        return fileName;
    }

    public String getcsvFolderPathFromExportSource(String exportSourceName) {

        String folderPath = server27FolderPath + File.separator + getCurretYear() + File.separator + getCurretMonth() + File.separator + exportSourceName;
        return folderPath;
    }

    public String getcsvFileNameForMaxMonthlyLimitBackupFileForLastMonth(String exportSourceName) {

        String fileName = File.separator + "firebrickgroup_" + exportSourceName + "_" + getLastMonthYear() + "_backup" + ".csv";
        return fileName;
    }

}
